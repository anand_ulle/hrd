import concurrent.futures
import glob,cv2,sys, datetime,time,os,torch,csv,numpy as np
from openslide import OpenSlide
import statistics
from torch.utils.data import Dataset, DataLoader
from albumentations.pytorch import ToTensorV2
import albumentations as A
import torch.nn as nn
from fastai.tabular.learner import load_learner
import matplotlib.pyplot as plt
import warnings
import tifffile
from decimal import *
import subprocess
import pandas as pd
import json, csv
import shutil
from hurry.filesize import size

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
transform = A.Compose([
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
    ])
class PatchDataset(Dataset):
    def __init__(self, valid_patch_list, transform=None):
        self.patch_list = valid_patch_list
        self.transform = transform

    def __len__(self):
        return len(self.patch_list)

    def __getitem__(self, index):
        img = self.patch_list[index]['img']
        o_indx = int(self.patch_list[index]['idx'])
        x = int(self.patch_list[index]['coord'][0])
        y = int(self.patch_list[index]['coord'][1])
        if self.transform:
            transformation = self.transform(image=img)
            img = transformation['image']
        return img.type(torch.FloatTensor), o_indx,x,y


def read_wsi_patch(idx,wsi,mask,patch_size,patch_pen):
    mean_grey_values = 0.8*255
    x, y = np.array(idx)
    x1, y1 = np.array(idx)//patch_size
    x2, y2 = x1*patch_pen,y1*patch_pen
    
    img = wsi.read_region((x, y), 0, (patch_size, patch_size))
    img = np.asarray(img.convert('RGB'))
    mask_img = mask[y2:y2+patch_pen, x2:x2+patch_pen]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    o_idx = wsi.level_dimensions[0][1]//patch_size*x1+y1
    if np.mean(gray) < mean_grey_values and np.any(mask_img):
        return {'img':img,'idx':o_idx,'coord':(x,y)}

def predict(valid_patch_list,mdl,BATCH_SIZE,savepath,fname): 
    learn_inf = load_learner(mdl)  
    model = learn_inf.model.eval().to(device)   
    softmax_layer = torch.nn.Softmax(dim=1)   
    final_model = nn.Sequential(model,softmax_layer)
    final_model= nn.DataParallel(final_model)
    final_model = final_model.to(device)    
    dataset = PatchDataset(valid_patch_list,transform=transform) 
    sampleloader = DataLoader(dataset=dataset, batch_size=BATCH_SIZE, shuffle=False)   
    json_data = []  
    
    with torch.no_grad():
        for _, (images, o_idx,x,y) in enumerate(sampleloader):  
            images = images.to(device)
            output = final_model(images)
            pred_prob, pred_class = output.max(axis=1)
            pred_class = pred_class.cpu().detach().numpy()
            pred_prob = pred_prob.cpu().detach().numpy()           
            # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started copying the json objects to a file {fname}')
            for i in range(len(pred_class)):
                deep_dict = {
                    "pred_class": pred_class[i],
                    "im_id": int(o_idx.numpy()[i]),
                    "pred_prob": float(pred_prob[i]),
                    "coord": (int(x.numpy()[i]),int(y.numpy()[i]))                    
                }                
                json_data.append(deep_dict)       
    return json_data

def pat_gen(wsi_file,mask_file,patch_size,fname,final_model,BATCH_SIZE,basepath,filename,savepath):
    mask=cv2.imread(mask_file)
    if mask is not None:
        wsi=OpenSlide(wsi_file)
        s = time.time()
        patch_pen = round((patch_size*mask.shape[0])/wsi.level_dimensions[0][1])
        
        coords0 = []
        for r in range(0, wsi.level_dimensions[0][0], patch_size):
            for c in range(0, wsi.level_dimensions[0][1], patch_size):
                coords0.append([r, c])
                
        with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
            futures = []
            for idx in coords0:
                futures.append(executor.submit(read_wsi_patch, idx, wsi, mask, patch_size, patch_pen))

        res = []
        for future in concurrent.futures.as_completed(futures):
            if future.result() is not None:
                res.append(future.result())
        res = sorted(res, key = lambda i: i['idx']) 

        # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Finished generating patches from WSI:{fname}')
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started Predicting {fname}')
        json_data=predict(res,final_model,BATCH_SIZE,savepath,fname)    
        df = pd.DataFrame(json_data)
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Done with Prediction of WSI:{fname}')      
        df.to_json(savepath + fname + '.json')
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Done copying json-data:{fname}')      
        # prob=avg_prob(json_data)
        sample_type,percent = get_sample_type(json_data)              
        t = (time.time()-s)/60                
        rec = [fname, str(size(os.path.getsize(basepath + fname))),
                str(np.round(percent, 2)), str(np.round(t, 2)),sample_type]        
        try:
            csvfile = open(filename, 'x')
            fields = ['Filename', 'File size', 'Pred_Prob', 'Pred_time','Final_Pred']
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
            csvwriter.writerow(rec)
            csvfile.close()            
        except FileExistsError:
            csvfile = open(filename, 'a')
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(rec)
            csvfile.close()
        
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Report Done {fname}')
        
    
    

def patches(basepath,savepath,final_model):
    patch_size = 256
    slideExt = '*.svs'
    patchExt = '*.png'
    filename = savepath + 'report.csv'
    BATCH_SIZE = 512
    wsi_file = sorted(glob.glob(basepath + slideExt))
    mask_file = sorted(glob.glob(basepath + patchExt))  
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:        
        for ind in range(len(wsi_file)):
            fname=wsi_file[ind].split('/')[-1] 
            print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started extracting patches {fname}')
            {executor.submit(pat_gen, wsi_file[ind],mask_file[ind],patch_size,fname,final_model,BATCH_SIZE,basepath,filename,savepath)}

   
def get_sample_type(json_data):
    fin_lst_cl0 = [item['pred_prob'] for item in json_data if item['pred_class']==0] 
    fin_lst_cl1 = [item['pred_prob'] for item in json_data if item['pred_class']==1] 
    
    DEN = (len(fin_lst_cl0)+len(fin_lst_cl1))   
    msi_ratio=len(fin_lst_cl0)/DEN
    mss_ratio=len(fin_lst_cl1)/DEN
    if(msi_ratio > mss_ratio):   
        cls = 'Homologous Recombination Deficient(HRD)'
        last_bin = [i for i in fin_lst_cl0 if i >= 0.9]
        avg_prob = np.mean(last_bin)       
    else:
        cls = 'Homologous Recombination Proficient(HRP)'        
        last_bin = [i for i in fin_lst_cl1 if i >= 0.9]
        avg_prob = np.mean(last_bin)        
    return cls,avg_prob


if __name__ == "__main__":
    
    args = sys.argv[1:]
    path = args[0]
    basepath = path + 'brca/data/imgs/test_data/hrp/'
    savepath = path + 'ova/bloom_tcga/results/brca/hrp/'
    model = path + 'ova/bloom_tcga/model/hrd_combined.pkl'
    patches(basepath,savepath,model)

