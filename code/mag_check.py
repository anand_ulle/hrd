import subprocess
import sys, pandas as pd
import shutil
import os, csv

def mag_check(PATH,output_dir):
    CONFIG_PATH = '/home/pmshiva/HistoQC/build/lib/histoqc/config/config_v2.1.ini'
    HISTOQC_PATH = '/home/pmshiva/HistoQC/build/lib/histoqc/__main__.py'
    os.mkdir(output_dir)
    sys.path.append('/usr/lib/python3.9')  # This is where default python package is intalled
    subprocess.run([sys.executable ,HISTOQC_PATH, '-c', CONFIG_PATH, '--force', '-o', output_dir, '-n', '10', PATH+'*.svs'])
    result = pd.DataFrame(csv.reader(open(str(output_dir) + '/results.tsv'), delimiter='\t'))
    result = (result.loc[6:,[0,2]]).values
    for i in range(len(result)):
        try:
            os.remove(PATH + str(result[i][0]+'_mask_use.png'))
        except OSError:
            pass

        if result[i][1] == '40.0':
            shutil.move(output_dir+str('/'+result[i][0]+'/'+result[i][0]+'_mask_use.png'), PATH)
        else:
            print('The Image is not of 40X magnification...')
            continue
        
    for i in os.listdir(output_dir):
        temp = os.path.join(output_dir,i)
    
    if (os.path.isfile(temp)):
        os.remove(temp)
    else:
        shutil.rmtree(temp)


if __name__ == "__main__":
    
    args = sys.argv[1:]
    basepath = args[0]  
    hqcpath = args[1] 
    mag_check(basepath,hqcpath)