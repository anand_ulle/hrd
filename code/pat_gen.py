import concurrent.futures
import glob,cv2,sys, datetime,time,os, numpy as np
from openslide import OpenSlide
import warnings, shutil
from pathlib import Path

def read_wsi_patch(idx, wsi, mask, patch_size, patch_pen, fname, savepath,label):
    mean_grey_values = 0.8*255

    x, y = np.array(idx)
    x1, y1 = np.array(idx)//patch_size
    x2, y2 = x1*patch_pen,y1*patch_pen
    
    im = wsi.read_region((x, y), 0, (patch_size, patch_size))
    img = np.asarray(im.convert('RGB'))
    mask_img = mask[y2:y2+patch_pen, x2:x2+patch_pen]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    o_idx = wsi.level_dimensions[0][1]//patch_size*x1+y1
    if np.mean(gray) < mean_grey_values and np.any(mask_img):
        im.save(savepath + fname + '_' + str(o_idx) + '.png')


def pat_gen(wsi_file,mask_file,patch_size,fname,savepath,label):
    mask=cv2.imread(mask_file)
    if mask is not None:
        wsi=OpenSlide(wsi_file)
        s = time.time()
        patch_pen = round((patch_size*mask.shape[0])/wsi.level_dimensions[0][1])
        
        coords0 = []
        for r in range(0, wsi.level_dimensions[0][0], patch_size):
            for c in range(0, wsi.level_dimensions[0][1], patch_size):
                coords0.append([r, c])
                
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
            futures = []
            for idx in coords0:
                {executor.submit(read_wsi_patch, idx, wsi, mask, patch_size, patch_pen, fname, savepath,label)}


def patches(label,basepath,savepath):
    
    patch_size = 256
    slideExt = '*.svs'
    patchExt = '*.png'
    s_path = basepath + 'training_dataset_bloom/' + label + '/' 
    # s_path = basepath + 'tmp/' + label + '/' 
    wsi_file = sorted(glob.glob(s_path +  slideExt))
    mask_file = sorted(glob.glob(s_path + patchExt))  
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:        
        for ind in range(len(wsi_file)):
            fname=wsi_file[ind].split('/')[-1].split('.')[0]
            print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started extracting {label} patches from {fname}')
            {executor.submit(pat_gen, wsi_file[ind],mask_file[ind],patch_size,fname,savepath,label)}
    print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Patch Extraction Done')


if __name__ == "__main__":
    
    args = sys.argv[1:]
    basepath = args[0]        
    with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
        class_name=['hrd','hrp']
        # class_name=['mss']  
        for label in class_name:
            # savepath = basepath + 'patches/'+ label + '/'
            savepath = basepath + 'train_patches_bloom/' + label + '/'
            if os.path.exists(savepath) and os.path.isdir(savepath):
                shutil.rmtree(savepath)
            os.makedirs(savepath)            
            {executor.submit(patches,label,basepath,savepath)}