#!/bin/bash
docker run -v "$1":/temp bioimaging/histoqc:latest python3 -m histoqc -c v2.1 -n 3 --force "/temp/$2" -o "/temp/HQC"
