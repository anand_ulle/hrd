HQC Module:
Steps to be followed to deploy HQC Module
1. Download HistoQC from https://github.com/choosehappy/HistoQC.git
        
2. With Docker: 
    A. Dockerizing
        Step1 :git clone https://github.com/choosehappy/HistoQC.git
        Step2 :cd HistoQC
        Step3 :Build the docker using Dockerfile
    B. Execuction:
        Step1 :sudo docker run -v {path to basepath where image folder is stored}:/temp bioimaging/histoqc:latest python3 -m histoqc -c v2.1 -n 3 "/temp/{File Extension Eg. *.svs} -o "/temp/HQC"
                image: Folder where images are loaded
                HQC: Folder where HQC o/p is recorded
        Step2 : Shell script to call the docker 
                bash hqc.sh {path to image} {file extension Eg.*.svs}
        
        