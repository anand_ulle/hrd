import concurrent.futures
import glob,cv2,sys, datetime,time,os,torch,csv,numpy as np
from openslide import OpenSlide
import statistics
from torch.utils.data import Dataset, DataLoader
from albumentations.pytorch import ToTensorV2
import albumentations as A
import torch.nn as nn
from fastai.tabular.learner import load_learner
import matplotlib.pyplot as plt
import warnings
import tifffile
from decimal import *
import subprocess
import pandas as pd
import json, csv
import shutil
from hurry.filesize import size

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
transform = A.Compose([
    A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
    ToTensorV2(),
    ])


class PatchDataset(Dataset):
    def __init__(self, valid_patch_list, transform=None):
        self.patch_list = valid_patch_list
        self.transform = transform

    def __len__(self):
        return len(self.patch_list)

    def __getitem__(self, index):
        img = self.patch_list[index]['img']
        o_indx = int(self.patch_list[index]['idx'])
        x = int(self.patch_list[index]['coord'][0])
        y = int(self.patch_list[index]['coord'][1])
        if self.transform:
            transformation = self.transform(image=img)
            img = transformation['image']

        return img.type(torch.FloatTensor), o_indx,x,y


def read_wsi_patch(idx,wsi,mask,patch_size,patch_pen):
    mean_grey_values = 0.8*255

    x, y = np.array(idx)
    x1, y1 = np.array(idx)//patch_size
    x2, y2 = x1*patch_pen,y1*patch_pen
    
    img = wsi.read_region((x, y), 0, (patch_size, patch_size))
    img = np.asarray(img.convert('RGB'))
    mask_img = mask[y2:y2+patch_pen, x2:x2+patch_pen]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    o_idx = wsi.level_dimensions[0][1]//patch_size*x1+y1
    if np.mean(gray) < mean_grey_values and np.any(mask_img):
        return {'img':img,'idx':o_idx,'coord':(x,y)}



def predict(valid_patch_list,mdl,BATCH_SIZE,savepath,fname): 

    learn_inf = load_learner(mdl)
    model = learn_inf.model.eval().to(device)
    softmax_layer = torch.nn.Softmax(dim=1)
    final_model = nn.Sequential(model,softmax_layer)
    final_model= nn.DataParallel(final_model)
    final_model = final_model.to(device)

    dataset = PatchDataset(valid_patch_list,transform=transform) 
    sampleloader = DataLoader(dataset=dataset, batch_size=BATCH_SIZE, shuffle=False)   
    json_data = []    
    with torch.no_grad():
        for _, (images, o_idx,x,y) in enumerate(sampleloader):  
            images = images.to(device)
            output = final_model(images)
            pred_prob, pred_class = output.max(axis=1)
            pred_class = pred_class.cpu().detach().numpy()
            pred_prob = pred_prob.cpu().detach().numpy()           
            # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started copying the json objects to a file {fname}')
            for i in range(len(pred_class)):
                deep_dict = {
                    "pred_class": pred_class[i],
                    "im_id": int(o_idx.numpy()[i]),
                    "pred_prob": float(pred_prob[i]),
                    "coord": (int(x.numpy()[i]),int(y.numpy()[i]))                    
                }
                json_data.append(deep_dict)       
    return json_data


def pat_gen(wsi_file,mask_file,patch_size,fname,final_model,BATCH_SIZE,basepath,filename,level,savepath):
    mask=cv2.imread(mask_file)
    if mask is not None:
        wsi=OpenSlide(wsi_file)
        s = time.time()
        patch_pen = round((patch_size*mask.shape[0])/wsi.level_dimensions[0][1])
        
        coords0 = []
        for r in range(0, wsi.level_dimensions[0][0], patch_size):
            for c in range(0, wsi.level_dimensions[0][1], patch_size):
                coords0.append([r, c])
                
        with concurrent.futures.ThreadPoolExecutor(max_workers=16) as executor:
            futures = []
            for idx in coords0:
                futures.append(executor.submit(read_wsi_patch, idx, wsi, mask, patch_size, patch_pen))

        res = []
        for future in concurrent.futures.as_completed(futures):
            if future.result() is not None:
                res.append(future.result())
        res = sorted(res, key = lambda i: i['idx']) 

        # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Finished generating patches from WSI:{fname}')
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started Predicting {fname}')
        json_data=predict(res,final_model,BATCH_SIZE,savepath,fname)    
        df = pd.DataFrame(json_data)
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Done with Prediction of WSI:{fname}')      
        df.to_json(savepath + fname + '.json')
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Done copying json-data:{fname}')      
        prob=avg_prob(json_data)
        sample_type,percent = get_sample_type(json_data)        
        t = (time.time()-s)/60
        rec = [fname+'.svs', str(size(os.path.getsize(basepath + fname+'.svs'))),
                str(np.round(percent, 2)), str(np.round(t, 2)),sample_type]
        try:
            csvfile = open(filename, 'x')
            fields = ['Filename', 'File size', 'Pred_Prob', 'Pred_time','Final_Pred']
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
            csvwriter.writerow(rec)
            csvfile.close()
        except FileExistsError:
            csvfile = open(filename, 'a')
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(rec)
            csvfile.close()
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Report Done {fname}')
        image,heatmap=down_scaled_im(wsi, patch_size, json_data, prob,level)
        print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Heatmap Done {fname}')
        visualize_heatmap(image,heatmap,fname,savepath)
      
def patches(basepath,savepath,final_model,level,slideExt):
    
    patch_size = 256
    patchExt = '*.png'
    filename = savepath + 'report.csv'
    BATCH_SIZE = 1024
    wsi_file = sorted(glob.glob(basepath + slideExt))
    mask_file = sorted(glob.glob(basepath + patchExt))  
    # print(len(wsi_file),len(mask_file))    
    # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Quality Check Done for all WSIs')
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:        
        for ind in range(len(wsi_file)):
            fname=wsi_file[ind].split('/')[-1].split('.')[0]
            # print(f'Working on {fname}')
            # print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started Quality Check')
            # mag_check(basepath)    
            print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Started extracting patches {fname}')
            {executor.submit(pat_gen, wsi_file[ind],mask_file[ind],patch_size,fname,final_model,BATCH_SIZE,basepath,filename,level,savepath)}


def avg_prob(deep_json):
    msi_probs = []
    for i in deep_json:
        if i['pred_class'] == 0: msi_probs.append(i['pred_prob'])
    return statistics.mean(msi_probs)


def visualize_heatmap(image,heatmap,fname,savepath):
    
    cm = plt.get_cmap('jet')
    colored_image = cm(heatmap)
    map=(colored_image[:, :, :3] * 255).astype(np.uint8)    
    fused_image = cv2.addWeighted(image,0.7,map,0.3,1)
    print(f'{datetime.datetime.now().replace(microsecond=0)}--[INFO]:Writing the image {fname} to the disk')
    tifffile.imwrite(savepath+ fname + '_original_image.tiff',image,bigtiff=True)
    tifffile.imwrite(savepath+ fname + '_overlay_image.tiff',heatmap,bigtiff=True)
    tifffile.imwrite(savepath+ fname + '_fuse_image.tiff',fused_image,bigtiff=True)

def down_scaled_im(wsi, patch_size, deep_json, msi_prob, level):
    '''
       This function takes whole slide image and create patches of desired size
       it also repatches(reconstruct) into a PIL image (RGBA)
       input arguments: whole slide image 
       Input Arguments: Whole slide image
       Output Arguments: reconstructed PIL image 
    '''
    indx = 0
    dwn_scale_patchSize=patch_size//(pow(2,level)*pow(2,level))
    img_tiles_d0 = []
    heat_tiles_d0 = []
    for r in range(0, wsi.level_dimensions[0][0], patch_size):
        img_tiles_d1 = []
        heat_tiles_d1 = []
        for c in range(0, wsi.level_dimensions[0][1], patch_size):
            o_indx = (wsi.level_dimensions[0][1]//patch_size)*(r//patch_size)+(c//patch_size)
            img = wsi.read_region((r, c), level, (dwn_scale_patchSize, dwn_scale_patchSize))
            img = np.asarray(img.convert('RGB'))
            if (indx < len(deep_json)) and (o_indx == int(deep_json[indx]['im_id'])):
                if deep_json[indx]['pred_class'] == 0:
                    p = float(deep_json[indx]['pred_prob'])
                    # if p < float(0.96):
                    if p < msi_prob:
                        p = 0.0
                elif deep_json[indx]['pred_class'] == 1:
                    p = 0.0
                indx += 1
            else:
                p = 0.0
            heat_patch = np.ones((img.shape[0], img.shape[1]))
            heat_patch = heat_patch*p
            img_tiles_d1.append(img)
            heat_tiles_d1.append(heat_patch)
        tmp1 = np.vstack(img_tiles_d1)
        tmp2 = np.vstack(heat_tiles_d1)
        img_tiles_d0.append(tmp1)
        heat_tiles_d0.append(tmp2)
    
    image = np.hstack(img_tiles_d0)
    heatmap = np.hstack(heat_tiles_d0)
    return image, heatmap


def validate_patches(wsi,res,patch_size,level):
    """
    This function is to validate the generated patches by generating a mask(preferably at level 2 or 3)
    made from the generated patches 
    """
    dwn_scale_patchSize=patch_size/(pow(2,level)*pow(2,level))
    indx=0
        
    img_tiles_d0 = []
    heat_tiles_d0 = []
    for r in range(0, wsi.level_dimensions[0][0], patch_size):
        img_tiles_d1 = []
        heat_tiles_d1 = []
        for c in range(0, wsi.level_dimensions[0][1], patch_size):
            o_indx = (wsi.level_dimensions[0][1]//patch_size)*(r//patch_size)+(c//patch_size)
            img = wsi.read_region((r, c), level, (dwn_scale_patchSize, dwn_scale_patchSize))
            img = np.asarray(img.convert('RGB'))
            if indx<len(res) and  o_indx == res[indx]['idx']:
                p=0.7
                indx += 1
            else:
                p = 0.0  
            heat_patch = np.ones((img.shape[0], img.shape[1]))
            heat_patch = heat_patch*p
            img_tiles_d1.append(img)
            heat_tiles_d1.append(heat_patch)
        tmp1 = np.vstack(img_tiles_d1)
        tmp2 = np.vstack(heat_tiles_d1)
        img_tiles_d0.append(tmp1)
        heat_tiles_d0.append(tmp2)
    image = np.hstack(img_tiles_d0)
    heatmap = np.hstack(heat_tiles_d0) 
    return image,heatmap


def get_sample_type(json_data):
    
    fin_lst_cl0 = [item['pred_prob'] for item in json_data if item['pred_class']==0]
    fin_lst_cl1 = [item['pred_prob'] for item in json_data if item['pred_class']==1]
    # print(len([i for i in fin_lst_cl0 if i >= 0.8]))
    # print(len([i for i in fin_lst_cl1 if i >= 0.8]))
    # print(len(fin_lst_cl0),len(fin_lst_cl1))
    hrd_ratio=len([i for i in fin_lst_cl0 if i >= 0.8])/len(fin_lst_cl0)
    nhrd_ratio=len([i for i in fin_lst_cl1 if i >= 0.8])/len(fin_lst_cl1)
    
    if(hrd_ratio > nhrd_ratio):
    # if len([i for i in fin_lst_cl0 if i >= 0.8]) > np.sum((len(fin_lst_cl0),len(fin_lst_cl1)))*0.10:        
        cls = 'Homologous Recombination Deficient'
        last_bin = [i for i in fin_lst_cl0 if i >= 0.9]
        avg_prob = np.mean(last_bin)
    else:
        cls = 'Homologous Recombination Proficient'        
        last_bin = [i for i in fin_lst_cl1 if i >= 0.9]
        avg_prob = np.mean(last_bin)
    return cls,avg_prob


if __name__ == "__main__":
    
    args = sys.argv[1:]
    basepath = args[0] # basepath = "/home/anand/bi-imaging-data/HRD_Project/data/images/test/" 
    savepath = args[1] # savepath = "/home/anand/bi-imaging-data/HRD_Project/results/new_results/"
    model= args[2]  # model = "/home/anand/bi-imaging-data/HRD_Project/model/hrd.pkl"
    slideExt = args[3] #slideExt is the whole slide image format Eg. *.svs, *.tiff
    level = 3  
    patches(basepath,savepath,model,level, slideExt)

