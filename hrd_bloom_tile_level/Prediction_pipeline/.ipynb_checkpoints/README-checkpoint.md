HRD Module:
Steps to be followed to deploying HRD Module
1. Create a python environment using requirements.txt 
    Usage: pip3 install -r requirements.txt
2. Execution:
    python3 wsi_predict.py {path to image} {path to o/p folder} {path to model} slideExt
    Note:
        path to image: Folder containing whole slide images and corresponding masks in png created by HQC module
        path to o/p folder: The folder where Prediction report, json file and heatmap images are saved
        path to model: folder containing the DL model for HRD (hrd.pkl)
        slideExt : *.svs or *.tiff or *.ndpi
    *requires GPU support for the execution